//
//  SchoolListViewModel.swift
//  20211030-ReemaBhalani-NYCSchools
//
//  Created by Reema on 10/31/21.
//

import Foundation

struct SchoolListViewModel {
    
    /// List of all schools summary
    var schools: [SchoolSummary] = []
    
    /// Indicates there was an error fetching list from server
    var hasError: Bool = false
    
    func loadHighSchoolList(
        session: URLSession = URLSession.shared,
        onComplete: @escaping(Self) -> Void
    ) {
        SchoolListService(session: session)
            .fetchHighSchoolList { (highSchools, error)  in
            guard error == nil else {
                let vm = SchoolListViewModel (
                   hasError: true
                )
                onComplete(vm)
                return
            }
            
            guard let schools = highSchools else {
                return
            }
            
            /// Creates new VM with sorted ascending by school names to return
            let vm = SchoolListViewModel(
                schools: schools.sorted {$0.schoolName < $1.schoolName}
            )
            onComplete(vm)
        }
    }
}
