//
//  SchoolDetailTableViewController.swift
//  20211030-ReemaBhalani-NYCSchools
//
//  Created by Reema on 10/31/21.
//

import UIKit

enum SATScore: Int, CaseIterable {
    case math
    case reading
    case writing
}

class SchoolDetailTableViewController: UITableViewController {

    lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .large
        activityIndicator.color = .gray
        activityIndicator.stopAnimating()
        return activityIndicator
    }()
    
    var schoolSummary: SchoolSummary!

    private let schoolDetailsReusableTableCell = "schoolDetailsReusableTableCell"

    var viewModel = SchoolDetailViewModel() {
        didSet {
            guard viewModel.hasError == false else {
                showAlert(message: "There is an issue loading school detail.")
                return
            }
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()

        viewModel.loadHighSchoolDetail(dbn: schoolSummary.dbn) { (viewModel) in
            DispatchQueue.main.async { [weak self] in
                self?.viewModel = viewModel
                self?.activityIndicator.stopAnimating()
            }
        }
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section == 1 else {
            return 1
        }
        
        return SATScore.allCases.count
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return "SAT Score"
        }
        
        return ""
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: schoolDetailsReusableTableCell, for: indexPath) as? TitleValueTableViewCell else {
            return UITableViewCell()
        }
        
        guard indexPath.section == 1 else {
            cell.titleLabel.text = "Total Students"
            cell.valueLabel.text = schoolSummary.totalStudents
            return cell
        }

        // Configure the cell...
        cell.configure(
            indexPath.row,
            schoolDetail: viewModel.schoolDetail
        )
            
        return cell
    }
}

private extension SchoolDetailTableViewController {
    
    func setupView() {
        configureNavigationTitle()
        
        configureFooterView()
        
        tableView.register(
            TitleValueTableViewCell.self,
            forCellReuseIdentifier: schoolDetailsReusableTableCell
        )
    }

    func configureNavigationTitle() {
        let label = UILabel()
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .center
        label.text = schoolSummary.schoolName
        navigationItem.titleView = label
    }
    
    @objc func moreInfoTapped() {
        if let website = schoolSummary.website,
           let url = URL(string: "https://\(website)") {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
            } else {
                showAlert(message: "There is an issue loading school website: \(website)")
            }
        }
    }
    
    func configureFooterView() {
        guard schoolSummary.website != nil else {
            return
        }

        let button = UIButton(type: .roundedRect)
        let padding = 10.0
        button.frame = CGRect(x: padding, y: padding, width: Double(view.bounds.width) - (padding*2), height: 60)
        button.setTitle("More Information", for: .normal)
        button.backgroundColor = .clear
        button.setTitleColor(.systemBlue, for: .normal)
        button.addTarget(self, action: #selector(moreInfoTapped), for: .touchUpInside)
        tableView.tableFooterView = button
    }


    func showAlert(message: String) {
        let alert = UIAlertController(title: "Error!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
}
