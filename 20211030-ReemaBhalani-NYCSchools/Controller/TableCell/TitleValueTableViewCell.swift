//
//  SchoolDetailTableViewCell.swift
//  20211030-ReemaBhalani-NYCSchools
//
//  Created by Reema on 10/31/21.
//

import UIKit

class TitleValueTableViewCell: UITableViewCell {

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var infoStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, valueLabel])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 8
        stackView.distribution = .fillEqually
        stackView.alignment = .leading
        stackView.axis = .vertical
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        infoStackView.removeFromSuperview()
        contentView.addSubview(infoStackView)
        setupConstraints()
    }

    func setupConstraints() {
        let padding: CGFloat = 20.0
        NSLayoutConstraint.activate([
            infoStackView.leadingAnchor.constraint(
                equalTo: contentView.safeAreaLayoutGuide.leadingAnchor,
                constant: padding
            ),
            infoStackView.trailingAnchor.constraint(
                equalTo: contentView.safeAreaLayoutGuide.trailingAnchor,
                constant: -padding
            ),
            infoStackView.topAnchor.constraint(
                equalTo: contentView.topAnchor,
                constant: padding
            ),
            infoStackView.bottomAnchor.constraint(
                equalTo: contentView.bottomAnchor,
                constant: -padding
            )
        ])
    }

    func configure(schoolSummary: SchoolSummary) {
        accessoryType = .disclosureIndicator
        titleLabel.text = schoolSummary.schoolName
        valueLabel.text = schoolSummary.neighborhood
    }
    
    func configure(_ row: Int, schoolDetail: SchoolDetail?) {
        switch row {
            case SATScore.math.rawValue:
                titleLabel.text = "Math"
                valueLabel.text = schoolDetail?.satMathScore
            case SATScore.reading.rawValue:
                titleLabel.text = "Reading"
                valueLabel.text = schoolDetail?.satReadingScore
            case SATScore.writing.rawValue:
                titleLabel.text = "Writing"
                valueLabel.text = schoolDetail?.satWritingScore
            default:
                break
        }
    }
}
