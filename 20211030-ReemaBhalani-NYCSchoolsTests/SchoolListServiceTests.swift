//
//  SchoolListServiceTests.swift
//  20211030-ReemaBhalani-NYCSchoolsTests
//
//  Created by Reema on 10/31/21.
//

import XCTest

@testable import _0211030_ReemaBhalani_NYCSchools

class SchoolListViewModelTests: XCTestCase {
    func testViewModelLoadingSchoolList() {
        let exp = expectation(description: "Loading schools")
        let vm = SchoolListViewModel()
        vm.loadHighSchoolList(session: MockURLSession()) { (vm) in
            XCTAssertEqual(vm.schools.count, 4)
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 2)
    }
}

class MockURLSession: URLSession {
    let mockData = """
    [
    {
    "dbn": "02M260",
    "school_name": "Clinton School Writers & Artists, M.S. 260",
    "neighborhood": "Chelsea-Union Sq",
    "website": "www.theclintonschool.net",
    "total_students": "376"
    },
    {
    "dbn": "21K728",
    "school_name": "Liberation Diploma Plus High School",
    "neighborhood": "Seagate-Coney Island",
    "website": "schools.nyc.gov/schoolportals/21/K728",
    "total_students": "206"
    },
    {
    "dbn": "08X282",
    "school_name": "Women's Academy of Excellence",
    "neighborhood": "Castle Hill-Clason Point",
    "website": "schools.nyc.gov/SchoolPortals/08/X282",
    "total_students": "338"
    },
    {
    "dbn": "17K548",
    "school_name": "Brooklyn School for Music & Theatre",
    "neighborhood": "Crown Heights South",
    "website": "www.bkmusicntheatre.com",
    "total_students": "352"
    }
    ]
    """.data(using: .utf8)
    
    override func dataTask(with url: URL, completionHandler: ((Data?, URLResponse?, Error?) -> Void)?) -> URLSessionDataTask {
        return MockURLSessionDataTask(
            url: url,
            completionHandler: completionHandler,
            mockData: mockData
        )
    }
}

class MockURLSessionDataTask: URLSessionDataTask {
    let url: URL
    let completionHandler: ((Data?, URLResponse?, Error?) -> Void)?
    let mockData: Data?
    
    init(url: URL, completionHandler: ((Data?, URLResponse?, Error?) -> Void)?, mockData: Data?) {
        self.url = url
        self.completionHandler = completionHandler
        self.mockData = mockData
    }
    
    override func resume() {
        var urlResponse: URLResponse? {
            return HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: [:])
        }
        
        completionHandler!(mockData, urlResponse, nil)
    }
}
