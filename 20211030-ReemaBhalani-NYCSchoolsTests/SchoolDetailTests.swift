//
//  SchoolDetailTests.swift
//  20211030-ReemaBhalani-NYCSchoolsTests
//
//  Created by Reema on 10/31/21.
//

import XCTest

@testable import _0211030_ReemaBhalani_NYCSchools

class SchoolDetailTests: XCTestCase {

    func testSchoolDetailValidModel() throws {
        let json = """
        {
            "dbn": "01M292",
            "school_name": "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
            "num_of_sat_test_takers": "29",
            "sat_critical_reading_avg_score": "355",
            "sat_math_avg_score": "404",
            "sat_writing_avg_score": "363"
        }
        """

        let schoolDetail = try? JSONDecoder().decode(SchoolDetail.self, from: json.data(using: .utf8)!)
        XCTAssertNotNil(schoolDetail)
        XCTAssertEqual(schoolDetail?.schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
        XCTAssertEqual(schoolDetail?.dbn, "01M292")
        XCTAssertEqual(schoolDetail?.satReadingScore, "355")
        XCTAssertEqual(schoolDetail?.satWritingScore, "363")
        XCTAssertEqual(schoolDetail?.satMathScore, "404")
    }

    /// Some scores missing case but still valid for others case
    func testSchoolDetailMathScoreMissingValidModel() throws {
        let json = """
        {
            "dbn": "01M292",
            "school_name": "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
            "num_of_sat_test_takers": "29",
            "sat_critical_reading_avg_score": "355",
            "sat_writing_avg_score": "363"
        }
        """

        let schoolDetail = try? JSONDecoder().decode(SchoolDetail.self, from: json.data(using: .utf8)!)
        XCTAssertNotNil(schoolDetail)
        XCTAssertEqual(schoolDetail?.schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
        XCTAssertEqual(schoolDetail?.dbn, "01M292")
        XCTAssertEqual(schoolDetail?.satReadingScore, "355")
        XCTAssertEqual(schoolDetail?.satWritingScore, "363")
        XCTAssertEqual(schoolDetail?.satMathScore, "not available")
    }
}
