//
//  SchoolSummary.swift
//  20211030-ReemaBhalani-NYCSchools
//
//  Created by Reema on 10/31/21.
//

import Foundation

/// Represents School Summary or information
public struct SchoolSummary: Codable, Equatable {
    /// dbn of this school
    let dbn: String
    
    /// Name of this school
    let schoolName: String
    
    /// Neighborhood of this school
    let neighborhood: String
    
    /// Website of this school
    let website: String?
    
    /// Total students at this school
    let totalStudents: String
    
    enum Key: String, CodingKey {
        case schoolName = "school_name"
        case dbn
        case neighborhood
        case website
        case totalStudents = "total_students"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        self.schoolName = try container.decode(String.self, forKey: .schoolName)
        self.dbn = try container.decode(String.self, forKey: .dbn)
        self.neighborhood = try container.decodeIfPresent(String.self, forKey: .neighborhood) ?? "-"
        self.website = try container.decodeIfPresent(String.self, forKey: .website)
        self.totalStudents = try container.decodeIfPresent(String.self, forKey: .totalStudents) ?? "-"
    }
}
