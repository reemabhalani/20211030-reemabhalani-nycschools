//
//  SchoolDetailService.swift
//  20211030-ReemaBhalani-NYCSchools
//
//  Created by Reema on 10/31/21.
//

import Foundation

/// Service to fetch details of schools, for given dbn
struct SchoolDetailService {
    
    let session: URLSession
    
    let dbn: String
    
    let absoluteURLString = "\(baseURLString)/f9bf-2cp4.json?$where=dbn="
    
    /// Constructor with Dependency Injection support for mocking URLSession for tests
    init(session: URLSession = URLSession.shared, dbn: String) {
        self.session = session
        self.dbn = dbn
    }
    
    func fetchHighSchoolDetail(onCompletion: @escaping (SchoolDetail?, Error?) -> Void) {
        guard let urlString = """
            \(absoluteURLString)"\(dbn)"
            """.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
              let url = URL(string: urlString) else {
            onCompletion(nil, NSError(domain: "100", code: 1, userInfo: ["error":"URL can not be constructed with given query"]))
            return
        }
                        
        let task = session.dataTask(with: url) {(data, response, error) in
            guard error == nil else {
                print("fetchHighSchoolDetail - Error from network: \(String(describing: error?.localizedDescription))")
                onCompletion(nil, error)
                return
            }
            
            guard let data = data else {
                debugPrint("fetchHighSchoolDetail - Success - Empty data")
                onCompletion(nil, nil)
                return
            }
            
            if let highSchoolsInfo = convertDataToHighSchoolDetail(data) {
                onCompletion(highSchoolsInfo, nil)
                // TODO: Cache the data on Success response and decoded success
            } else {
                let err = "Error decoding response data to SchoolSummary"
                print("\(err)")
                onCompletion(nil, NSError(domain: "Service", code: 1, userInfo: ["Error":"\(err)"]))
            }
        }
        
        task.resume()
    }
    
    private func convertDataToHighSchoolDetail(_ data: Data) -> SchoolDetail? {
        do {
            let schoolDetail = try JSONDecoder().decode([SchoolDetail].self, from: data)
            
            return schoolDetail.first
            
        } catch let error as NSError {
            print("fetchEmployeeDetail - failed to decode: \(error.localizedDescription)")
        }
        
        return nil
    }
}
