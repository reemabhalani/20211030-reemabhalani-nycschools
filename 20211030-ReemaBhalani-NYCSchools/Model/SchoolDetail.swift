//
//  SchoolDetail.swift
//  20211030-ReemaBhalani-NYCSchools
//
//  Created by Reema on 10/31/21.
//

import Foundation

/// Represents School details including SAT score information
public struct SchoolDetail: Codable, Equatable {
    /// Name of this school
    let schoolName: String
    
    /// DBN of this school
    let dbn: String
    
    /// SAT critical reading average score
    let satReadingScore: String
    
    /// SAT math average score
    let satMathScore: String
    
    /// SAT writing average score
    let satWritingScore: String
    
    enum Key: String, CodingKey {
        case schoolName = "school_name"
        case dbn
        case satReadingScore = "sat_critical_reading_avg_score"
        case satMathScore = "sat_math_avg_score"
        case satWritingScore = "sat_writing_avg_score"
    }
    
    public init(from decoder: Decoder) throws {
        let notAvailable = "not available"

        let container = try decoder.container(keyedBy: Key.self)
        
        self.schoolName = try container.decode(String.self, forKey: .schoolName)
        
        self.dbn = try container.decode(String.self, forKey: .dbn)
                
        self.satReadingScore = try container.decodeIfPresent(String.self, forKey: .satReadingScore) ?? notAvailable
        
        self.satMathScore = try container.decodeIfPresent(String.self, forKey: .satMathScore) ?? notAvailable
        
        self.satWritingScore = try container.decodeIfPresent(String.self, forKey: .satWritingScore) ?? notAvailable
    } 
}
