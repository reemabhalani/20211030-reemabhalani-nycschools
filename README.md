# 20211030-ReemaBhalani-NYCSchools

## Name
NYC Schools list

## Description
### Tools
This app was built using XCode 12, with iOS deployment target 14.0

### Architecture 
- MVVM Architecture,
- Programmatic UI elements with constraints for Table Cell,
- UI with loading indicator and error popup,
- Unit testing Model and SchoolListViewModel/Service with mock session

## Authors 
Reema Bhalani

## Project status
Done two screens(Schools list, School SAT score details) and unit tests
