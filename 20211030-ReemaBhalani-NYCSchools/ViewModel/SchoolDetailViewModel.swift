//
//  SchoolDetailViewModel.swift
//  20211030-ReemaBhalani-NYCSchools
//
//  Created by Reema on 10/31/21.
//

import Foundation

struct SchoolDetailViewModel {
    
    var schoolDetail: SchoolDetail? = nil
    
    /// Indicates there was an error fetching list from server
    var hasError: Bool = false
    
    func loadHighSchoolDetail(
        session: URLSession = URLSession.shared,
        dbn: String,
        onComplete: @escaping(Self) -> Void
    ) {
        SchoolDetailService(
            session: session,
            dbn: dbn
        )
        .fetchHighSchoolDetail { (schoolInfo, error)  in
            guard error == nil else {
                let vm = SchoolDetailViewModel (
                    hasError: true
                )
                onComplete(vm)
                return
            }
            
            guard let schoolInfo = schoolInfo else {
                return
            }
            
            let vm = SchoolDetailViewModel(schoolDetail: schoolInfo)
            onComplete(vm)
        }
    }
}
