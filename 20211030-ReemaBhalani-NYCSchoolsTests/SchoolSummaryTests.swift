//
//  SchoolListTests.swift
//  20211030-ReemaBhalani-NYCSchoolsTests
//
//  Created by Reema on 10/31/21.
//

import XCTest
@testable import _0211030_ReemaBhalani_NYCSchools

class SchoolSummaryTests: XCTestCase {

    func testSchoolSummaryValidModel() throws {
        let json = """
        {
            "dbn": "02M260",
            "school_name": "Clinton School Writers & Artists, M.S. 260",
            "neighborhood": "Chelsea-Union Sq",
            "website": "www.theclintonschool.net",
            "total_students": "376"
        }
        """

        let schoolSummary = try? JSONDecoder().decode(SchoolSummary.self, from: json.data(using: .utf8)!)
        XCTAssertNotNil(schoolSummary)
        XCTAssertEqual(schoolSummary?.schoolName, "Clinton School Writers & Artists, M.S. 260")
        XCTAssertEqual(schoolSummary?.dbn, "02M260")
        XCTAssertEqual(schoolSummary?.neighborhood, "Chelsea-Union Sq")
        XCTAssertEqual(schoolSummary?.website, "www.theclintonschool.net")
    }

    /// Name is missing case, which as required field should failed to decode to model
    func testSchoolSummaryInvalidModel() throws {
        let json = """
        {
            "dbn": "02M260",
            "neighborhood": "Chelsea-Union Sq",
            "website": "www.theclintonschool.net",
            "total_students": "376"
        }
        """

        let schoolSummary = try? JSONDecoder().decode(SchoolSummary.self, from: json.data(using: .utf8)!)
        XCTAssertNil(schoolSummary)
    }
}
