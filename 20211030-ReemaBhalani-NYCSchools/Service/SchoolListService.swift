//
//  SchoolListService.swift
//  20211030-ReemaBhalani-NYCSchools
//
//  Created by Reema on 10/31/21.
//

import Foundation

let baseURLString = "https://data.cityofnewyork.us/resource"

/// Service to fetch list of schools
struct SchoolListService {
    let session: URLSession
    
    let absolutePath = "\(baseURLString)/s3k6-pzi2.json"
    
    let columnsSelectionQuery = "$select=dbn,school_name,neighborhood,website,total_students"
    
    /// Constructor with Dependency Injection support for mocking URLSession for tests
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    func fetchHighSchoolList(
        onCompletion: @escaping ([SchoolSummary]?, Error?) -> Void
    ) {
        
        let url = URL(
            string: "\(absolutePath)?\(columnsSelectionQuery)"
        )!
        
        let task = self.session.dataTask(with: url) {(data, response, error) in
            guard error == nil else {
                print("fetchHighSchoolList - Error from network: \(String(describing: error?.localizedDescription))")
                onCompletion([], error)
                return
            }
            
            guard let data = data else {
                debugPrint("fetchHighSchoolList - Success - Empty data")
                onCompletion([], nil)
                return
            }
            
            if let schoolList = convertDataToHighSchoolName(data) {
                onCompletion(schoolList, nil)
                // TODO: Cache the data on Success response
                
            } else {
                let err = "Error decoding response data to SchoolSummary"
                print("\(err)")
                onCompletion([], NSError(domain: "Service", code: 1, userInfo: ["Error":"\(err)"]))
            }
        }
        
        task.resume()
    }
    
    private func convertDataToHighSchoolName(_ data: Data) -> [SchoolSummary]? {
        do {
            let highSchoolList = try JSONDecoder().decode([SchoolSummary].self, from: data)
            
            return highSchoolList
            
        } catch let error as NSError {
            print("fetchEmployeeList - failed to decode: \(error.localizedDescription)")
        }
        
        return nil
    }
}
