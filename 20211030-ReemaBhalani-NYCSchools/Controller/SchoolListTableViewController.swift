//
//  SchoolListTableViewController.swift
//  20211030-ReemaBhalani-NYCSchools
//
//  Created by Reema on 10/31/21.
//

import UIKit

class SchoolListTableViewController: UITableViewController {
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .large
        activityIndicator.color = .gray
        activityIndicator.stopAnimating()
        return activityIndicator
    }()
    
    private let schoolListReusableTableCell = "schoolListReusableTableCell"
    
    private var viewModel = SchoolListViewModel() {
        didSet {
            guard viewModel.hasError == false else {
                showAlertFetchingList()
                return
            }
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "NYC High Schools"
        setupView()
        
        /// Async start fetching the employee list every-time on-start up,
        /// once only per run-cycle
        viewModel.loadHighSchoolList { (viewModel) in
            DispatchQueue.main.async { [weak self] in
                self?.viewModel = viewModel
                self?.activityIndicator.stopAnimating()
            }
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.schools.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: schoolListReusableTableCell,
            for: indexPath
        ) as? TitleValueTableViewCell else {
            return UITableViewCell()
        }

        cell.configure(schoolSummary: viewModel.schools[indexPath.row])
        
        return cell
    }
    
    override func tableView(
        _ tableView: UITableView, didSelectRowAt indexPath: IndexPath
    ) {
        let detailVC = SchoolDetailTableViewController()
        detailVC.schoolSummary = viewModel.schools[indexPath.row]
        navigationController?
            .pushViewController(
                detailVC,
                animated: true
            )    }
}

private extension SchoolListTableViewController {
    func setupView() {
        tableView.register(
            TitleValueTableViewCell.self,
            forCellReuseIdentifier: schoolListReusableTableCell
        )

        /// Add spinner view to show while loading at startup
        view.addSubview(activityIndicator)
        activityIndicator.center = view.center
        activityIndicator.startAnimating()
    }

    func showAlertFetchingList() {
        let alert = UIAlertController(title: "Error!", message: "There is an issue loading school list.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
}

